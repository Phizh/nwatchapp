//
//  nano3App.swift
//  nano3 WatchKit Extension
//
//  Created by Vincentius Phillips Zhuputra on 16/08/21.
//

import SwiftUI

@main
struct nano3App: App {
    @SceneBuilder var body: some Scene {
        WindowGroup {
            NavigationView {
                ContentView()
            }
        }

        WKNotificationScene(controller: NotificationController.self, category: "myCategory")
    }
}
